<?php

$compte_a_ouvrir=1;
function identification(&$clients,&$compte_a_ouvrir,&$id_possible2,&$donnees){
    echo (" ------------------------ CREER UN COMPTE ------------------------ \n");    
    echo("\n Avez vous un identifiant ? oui/non ");
    $id_possible = strtolower(readline( " : "));
    if($id_possible != "oui" && $id_possible != "non"){
        while($id_possible != "oui" && $id_possible != "non"){
            $id_possible = strtolower(readline((" Veuillez saisir oui/non : ")));
        }
    }
    if($id_possible == "non"){
    echo"\n Veuillez dabord crée un client !!! \n";
    creationClient($clients,$donnees);
    return;
    }
    elseif($id_possible== "oui"){
    $id_possible2=readline("   - Veuillez saisir votre identifiant : ");
    echo("\n");
    $clo=0;
        for($i=0;$i<=count($clients);$i++){
            if ($clo==count($clients)){
                echo(" Identifiant inconnu !!! ");
                break;
        }
        if($id_possible2== $clients[$i]->getId()){
            echo("           Quel type de compte souhaitez vous ouvrir ? \n
            A - Compte courant\n
            B - Livret A\n
            C - Plan epargne logement \n");
            echo("\n");
            $compte_a_ouvrir = strtolower(readline(" Votre choix : "));
            if($compte_a_ouvrir != "a" && $compte_a_ouvrir != "b" && $compte_a_ouvrir != "c"){
                while($compte_a_ouvrir != "a" && $compte_a_ouvrir != "b" && $compte_a_ouvrir != "c"){
                    $compte_a_ouvrir = strtolower(readline((" Saisie invalid : ")));
                }
            }
                return $compte_a_ouvrir;
                return $id_possible2;
        }
        $clo++;
    }
    }
}
function choixA(&$comptes,$agences,$clients,$id_possible2,&$donnees1){
    for($i=0;$i<count($comptes);$i++){
        if($comptes[$i]->getId() == $id_possible2 && $comptes[$i]->getType() =="Compte courant"){
            echo ("\n Vous avez deja un compte courant !!! \n");
            return;   
        }
    }
    $compte["codeagence"] = readline(" Veuillez saisir le code de l'agence à 3 chiffres : "); 
    $cppt=0;
    for($i=0;$i<count($agences);$i++)
    {
        if($agences[$i]->getCode() == $compte["codeagence"])break;
        else {
            $cppt++;
        }
    } 
    if($cppt == count($agences)){
        echo(" Agences inconnu !!!");
        return "!";
    }
    for($i=0;$i<count($clients);$i++){
        if($clients[$i]->getId()==$id_possible2){
            $compte["numero"]=numero_aleatoire($donnees1);
            $compte["identifiant"] = $id_possible2;
            $compte["solde"] = readline(" - Solde du client : \n");
            if (!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                while(!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                    $compte["solde"] = readline(" Veuillez saisir un solde valide : ");
                }
            }
            $compte["type"]="Compte courant";
        }
    }    
    $comptes[]=new Compte($compte["codeagence"],$compte["numero"],$compte["identifiant"],$compte["solde"],$compte["type"]);
    echo ("\n                      !!!  Voici votre nouveau compte  !!! \n");
    echo("\n");
    echo("                               - Numero : ".$compte["numero"]."           \n");
    echo("                               - Identifiant : ".$compte["identifiant"]."           \n");
    echo("                                   - Type : ".$compte["type"]."                   \n");
    echo("                                  - Solde : ".$compte["solde"]." Euro(s).               \n ");
}
function choixB(&$comptes,$agences,$clients,$id_possible2,&$donnees1){
    for($i=0;$i<count($comptes);$i++){
        if($comptes[$i]->getId() == $id_possible2 && $comptes[$i]->getType() =="Livret A"){
            echo ("\n Vous avez deja un compte courant !!! \n");
            return;   
        }
    }
    $compte["codeagence"] = readline(" Veuillez saisir le code de l'agence à 3 chiffres : "); 
    $cppt=0;
    for($i=0;$i<count($agences);$i++)
    {
        if($agences[$i]->getCode() == $compte["codeagence"])break;
        else {
            $cppt++;
        }
    } 
    if($cppt == count($agences)){
        echo(" Agences inconnu !!!");
        return "!";
    }
    for($i=0;$i<count($clients);$i++){
        if($clients[$i]->getId()==$id_possible2){
            $compte["numero"]=numero_aleatoire($donnees1);
            $compte["identifiant"] = $id_possible2;
            $compte["solde"] = readline(" - Solde du client : \n");
            if (!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                while(!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                    $compte["solde"] = readline(" Veuillez saisir un solde valide : ");
                }
            }
            $compte["type"]="Livret A";
        }
    }    
    $comptes[]=new Compte($compte["codeagence"],$compte["numero"],$compte["identifiant"],$compte["solde"],$compte["type"]);
    echo ("\n                      !!!  Voici votre nouveau compte  !!! \n");
    echo("\n");
    echo("                               - Numero : ".$compte["numero"]."           \n");
    echo("                               - Identifiant : ".$compte["identifiant"]."           \n");
    echo("                                   - Type : ".$compte["type"]."                   \n");
    echo("                                  - Solde : ".$compte["solde"]." Euro(s).               \n ");
}
function choixC(&$comptes,$agences,$clients,$id_possible2,&$donnees1){
    for($i=0;$i<count($comptes);$i++){
        if($comptes[$i]->getId() == $id_possible2 && $comptes[$i]->getType() =="Plan Epargne"){
            echo ("\n Vous avez deja un compte courant !!! \n");
            return;   
        }
    }
    $compte["codeagence"] = readline(" Veuillez saisir le code de l'agence à 3 chiffres : "); 
    $cppt=0;
    for($i=0;$i<count($agences);$i++)
    {
        if($agences[$i]->getCode() == $compte["codeagence"])break;
        else {
            $cppt++;
        }
    } 
    if($cppt == count($agences)){
        echo(" Agences inconnu !!!");
        return "!";
    }
    for($i=0;$i<count($clients);$i++){
        if($clients[$i]->getId()==$id_possible2){
            $compte["numero"]=numero_aleatoire($donnees1);
            $compte["identifiant"] = $id_possible2;
            $compte["solde"] = readline(" - Solde du client : \n");
            if (!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                while(!preg_match("#^\-?[0-9]*$#",$compte["solde"])){
                    $compte["solde"] = readline(" Veuillez saisir un solde valide : ");
                }
            }
            $compte["type"]="Plan Epargne";
        }
    }    
    $comptes[]=new Compte($compte["codeagence"],$compte["numero"],$compte["identifiant"],$compte["solde"],$compte["type"]);
    echo ("\n                      !!!  Voici votre nouveau compte  !!! \n");
    echo("\n");
    echo("                               - Numero : ".$compte["numero"]."           \n");
    echo("                               - Identifiant : ".$compte["identifiant"]."           \n");
    echo("                                   - Type : ".$compte["type"]."                   \n");
    echo("                                  - Solde : ".$compte["solde"]." Euro(s).               \n ");
}
