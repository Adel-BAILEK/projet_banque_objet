<?php


function creationAgence(&$agences, $donnees3)
{
    echo ("\n");
    echo (" ------------------------ CREER UNE AGENCE ------------------------ \n");
    $agence["nom"] = (strtolower(readline(" - Nom de l'agence : \n")));
    if (!preg_match("#^[a-zA-Z]+$#", $agence["nom"])) {
        while (!preg_match("#^[a-zA-Z]*$#", $agence["nom"])) {
            $agence["nom"] = readline(" Veuillez saisir un nom valide : ");
        }
    }
    $agence["code"] = code_agence($donnees3);
    $agence["adresse"] = readline(" - Adresse de l'agence : \n");
    $agence["postal"] = readline(" - Code postal de l'agence : \n");
    if (!preg_match("#^[0-9]{5}$#", $agence["postal"])) {
        while (!preg_match("#^[0-9]{5}$#", $agence["postal"])) {
            $agence["postal"] = readline(" Veuillez saisir un code postal valide : ");
        }
    }
    $agences[] =  new Agence($agence["nom"], $agence["code"], $agence["adresse"], $agence["postal"]);
    creationCsvAgences($agences);
    echo ("\n                      !!!  Voici votre nouvelle agence !!! \n");
    echo ("\n");
    echo ("                               - Nom : " . $agence["nom"] . "                   \n");
    echo ("                               - Code : " . $agence["code"] . "                  \n ");
    echo ("                              - Adresse : " . $agence["adresse"] . "               \n ");
    echo ("                              - Postal : " . $agence["postal"] . "                \n");
    echo ("\n");
}
function creationClient(&$clients, &$donnees)
{
    echo ("\n");
    echo (" ------------------------ CREER UN CLIENT ------------------------ \n");
    $client["nom"] = strtoupper((readline(" - Nom client : \n")));
    if (!preg_match("#^[a-zA-Z]*$#", $client["nom"])) {
        while (!preg_match("#^[a-zA-Z]*$#", $client["nom"])) {
            $client["nom"] = readline(" Veuillez saisir un nom valide : ");
        }
    }
    $client["prenom"] = ucwords(strtolower(readline(" - Prenom client : \n")));
    if (!preg_match("#^[a-zA-Z]*$#", $client["prenom"])) {
        while (!preg_match("#^[a-zA-Z]*$#", $client["prenom"])) {
            $client["prenom"] = ucwords(strtolower(readline(" Veuillez saisir un prénom valide : ")));
        }
    }
    $client["naissance"] = readline(" - Date de naissance (JJMMAAAA) : \n");
    if (!preg_match("#^(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[0-9]|1[0-2])\/(19[0-9]{2}|20[0-1]{1}[0-9]{1})$#", $client["naissance"])) {
        while (!preg_match("#^(0?[1-9]|[1-2][0-9]|3[0-1])\/(0?[0-9]|1[0-2])\/(19[0-9]{2}|20[0-1]{1}[0-9]{1})$#", $client["naissance"])) {
            $client["naissance"] = readline(" Veuillez saisir une date valide : ");
        }
    }
    $client["identifiant"] = strtoupper(code_aleatoire($donnees));
    $client["mail"] = readline(" - Mail du client : \n");
    if (!preg_match("#^[a-zA-Z0-9]+\.?[a-zA-Z0-9]+\@[a-z0-9]+\.[a-z]{2,4}$#", $client["mail"])) {
        while (!preg_match("#^[a-zA-Z0-9]+\.?[a-zA-Z0-9]+\@[a-z0-9]+\.[a-z]{2,4}$#", $client["mail"])) {
            $client["mail"] = readline(" Veuillez saisir un mail valide : ");
        }
    }
    foreach ($clients as $key => $valeur) {
        if ($clients[$key]->getMail() == $client["mail"]) {
            echo "\n Mail déja utilisé !";
            return;
        }
    }
    $decouvert = strtolower(readline(" - Decouvert autorisé oui/non : \n"));
    while ($decouvert != "oui" && $decouvert != "non") {
        $decouvert = strtolower(readline(" - Veuillez saisir oui ou non : "));
    }
    $client["decouvert"] = $decouvert;
    $clients[] = new Client(
        $client["nom"],
        $client["prenom"],
        $client["naissance"],
        $client["identifiant"],
        $client["mail"],
        $client["decouvert"]
    );
    echo ("\n                      !!!  Voici votre nouveau client !!! \n");
    echo ("\n");
    echo ("                               - Identifiant : " . $client["identifiant"] . "           \n");
    echo ("                                   - Nom : " . $client["nom"] . "                   \n");
    echo ("                                   - Prénom : " . $client["prenom"] . "                  \n ");
    echo ("                                  - Mail : " . $client["mail"] . "                \n");
    echo ("                                  - Découvert : " . $client["decouvert"] . "                \n");
    echo ("\n");
    creationCsvClients($clients);
}
function creationCompte(&$clients, $compte_a_ouvrir, &$comptes, &$donnees1, $agences, $id_possible2, &$donnees)
{
    identification($clients, $compte_a_ouvrir, $id_possible2, $donnees);
    if ($compte_a_ouvrir == "a") {
        choixA($comptes, $agences, $clients, $id_possible2, $donnees1);
    }
    if ($compte_a_ouvrir == "b") {
        choixB($comptes, $agences, $clients, $id_possible2, $donnees1);
    }
    if ($compte_a_ouvrir == "c") {
        choixC($comptes, $agences, $clients, $id_possible2, $donnees1);
    }
    creationCsvComptes($comptes);
}
function rechercheCompte($comptes)
{
    echo ("-------------------  RECHERCHE COMPTE -----------------");
    echo ("\n");
    $recherche = readline(" Veuillez saisir votre numéro de compte (11 chiffres) : ");
    $cpt = 0;
    for ($i = 0; $i < count($comptes); $i++) {
        if ($recherche == $comptes[$i]->getNumero()) {
            echo (" Compte trouvé \n");
            echo ("                               - Numero : " . $comptes[$i]->getNumero() . "           \n");
            echo ("                               - Identifiant : " . $comptes[$i]->getId() . "           \n");
            echo ("                                   - Type : " . $comptes[$i]->getType() . "                   \n");
            echo ("                                  - Solde : " . $comptes[$i]->getSolde() . " Euro(s).               \n ");
            return;
        }
        $cpt++;
    }
    if ($cpt == (count($comptes))) {
        echo ("\n Compte non existant !!! ");
        return;
    }
}
function recherche_client($clients, $comptes)
{
    echo ("-------------------  RECHERCHE CLIENT -----------------");
    echo ("\n");
    echo ("           Quel type de recherche souhaitez vous faire ? \n
            A - Recherche par nom\n
            B - Recherche par identifiant\n
            C - Numéro de compte\n");
    echo ("\n");
    $choix = strtoupper(readline(" Votre choix : "));
    echo ("\n");
    if ($choix == "A") {
        $nomr = strtoupper((readline(" - Nom client : \n")));
        $cptnom = 0;
        for ($i = 0; $i < count($clients); $i++) {
            if ($nomr == $clients[$i]->getNom()) {
                echo (" Compte trouvé \n");
                echo "                     - Identifiant: " . $clients[$i]->getId() . "           \n";
                echo "                     - Nom : " . $clients[$i]->getNom() . "           \n";
                echo "                     - Prénom : " . $clients[$i]->getPrenom() . "                    \n";
                echo "                     - Naissance : " . $clients[$i]->getNaissance() . "           \n";
                echo "                     - Mail : " . $clients[$i]->getMail() . "                \n  ";
                return;
            } else {
                $cptnom++;
            }
        }
        if ($cptnom == count($clients)) {
            echo (" \n Recherche par nom incorrect !!! \n");
            return;
        }
    }
    if ($choix == "B") {
        $nomr = strtoupper((readline(" - Identifiant client : \n")));
        $cptnom = 0;
        for ($i = 0; $i < count($clients); $i++) {
            if ($nomr == $clients[$i]->getId()) {
                echo ("\n");
                echo (" Compte trouvé \n");
                echo "                     - Identifiant: " . $clients[$i]->getId() . "           \n";
                echo "                     - Nom : " . $clients[$i]->getNom() . "           \n";
                echo "                     - Prénom : " . $clients[$i]->getPrenom() . "                    \n";
                echo "                     - Naissance : " . $clients[$i]->getNaissance() . "           \n";
                echo "                     - Mail : " . $clients[$i]->getMail() . "                \n  ";
                return;
            } else {
                $cptnom++;
            }
        }
        if ($cptnom == count($clients)) {
            echo (" \n Recherche par identifiant incorrect !!! \n");
            return;
        }
    }
    if ($choix == "C") {
        $nomr = strtoupper((readline(" - Numero de compte : \n")));
        $cptnom = 0;
        for ($i = 0; $i < count($comptes); $i++) {
            if ($nomr == $comptes[$i]->getNumero()) {
                echo ("\n");
                echo (" Compte trouvé \n");
                echo "                     - Identifiant: " . $clients[$i]->getId() . "           \n";
                echo "                     - Nom : " . $clients[$i]->getNom() . "                           \n";
                echo "                     - Prénom : " . $clients[$i]->getPrenom() . "                   \n";
                echo "                     - Naissance : " . $clients[$i]->getNaissance() . "           \n";
                echo "                     - Mail : " . $clients[$i]->getMail() . "                  \n";
                return;
            } else {
                $cptnom++;
            }
        }
        if ($cptnom == count($clients)) {
            echo (" \n Recherche par Numéro de compte incorrect !!! \n");
            return;
        }
    }
}
function recherche_compte_client($clients, $comptes)
{
    echo ("-------------------  RECHERCHE COMPTE CLIENT -----------------");
    echo ("\n");
    $cop = 0;
    $recherche = strtoupper(readline(" Veuillez saisir l'identifiant du client : "));
    foreach ($clients as $valeur) {
        if ($valeur->getId() == $recherche) {
            echo ("\n");
            echo (" Infos client : \n");
            echo ("                 - Identifiant : " . $valeur->getId() . "            \n");
            echo ("                 - Nom : " . $valeur->getNom() . "            \n");
            echo ("                 - Prénom : " . $valeur->getPrenom() . "            \n");
            echo ("                 - Naissance (JJMMAAA) : " . $valeur->getNaissance() . "            \n");
        } else {
            $cop++;
        }
    }
    if ($cop == count($clients)) {
        echo ("\n Client inexistant \n ");
    }
    echo ("\n");
    echo (" Listes de(s) compte(s) : \n");
    $e = 1;
    $ccp = 0;
    foreach ($comptes as $valeur) {
        if ($valeur->getId() == $recherche) {
            echo (" - COMPTE N°" . $e . " : " . "\n" . "      - Numero : " . $valeur->getNumero() . "\n" . "      - Solde : " . $valeur->getSolde() . "\n" . "      - Type : " . $valeur->getType() . "\n");
            $e++;
        } else {
            $ccp++;
        }
    }
    if ($ccp == count($comptes)) {
        echo ("\n Aucun compte \n");
        return;
    }
}
function imprimer_info($clients, $comptes)
{
    echo ("\n");
    $recherche = strtoupper(readline(" - Veuillez saisir l'identifiant client : "));
    for ($i = 0; $i < count($clients); $i++) {
        if ($recherche == $clients[$i]->getId()) {
            file_put_contents('./AImprimer/Fiche_client.txt', "------------------------------ FICHE CLIENT ------------------------------ \n "
                . "\n" .
                " - Identifiant client : " . $clients[$i]->getId() . "\n" .
                " - Nom : " . $clients[$i]->getNom() . "\n" .
                " - Prenom : " . $clients[$i]->getPrenom() . "\n" .
                " - Date de naissance : " . $clients[$i]->getNaissance() . "\n" .
                "\n" .
                "\n" .
                "--------------------------------------------------------------------------" . "\n" .
                " Liste de compte " . "\n" .
                "--------------------------------------------------------------------------" . "\n" .
                " Numéro de compte                                      Solde : " . "\n" .
                "--------------------------------------------------------------------------" . "\n");
            $fichier = file_get_contents('./AImprimer/Fiche_client.txt');
            foreach ($comptes as $valeur) {
                if ($valeur->getId() == $recherche) {
                    if ($valeur->getSolde() >= 0) {
                        $smile = ":-)";
                    } else {
                        $smile = ":-(";
                    }

                    $i = 0;
                    $fichier1 = " " . $valeur->getNumero() . "                                             " . $valeur->getsolde() . "Euro(s)   " . $smile . "\n";
                    $fichier .= $fichier1;
                    $i++;
                }
            }
            file_put_contents('./AImprimer/Fiche_client.txt', $fichier);

            echo ("\n      Votre fichier est pret a l'impression dans votre dossier  !!! \n");
            return;
        }
    }
    echo ("\n Identifiant inconnu !!!  \n");
}
function creationCsvClients($clients)
{
    $fp = fopen("./CSV/infos_clients.csv", "w+");
    foreach ($clients as $fields) {
        $clientX = [];
        $clientX[] = $fields->getNom();
        $clientX[] = $fields->getPrenom();
        $clientX[] = $fields->getNaissance();
        $clientX[] = $fields->getId();
        $clientX[] = $fields->getMail();
        $clientX[] = $fields->getDecouvert();
        fputcsv($fp, $clientX, ";");
    }
    fclose($fp);
}
function creationCsvAgences($agences)
{
    $fp = fopen("./CSV/infos_agences.csv", "w+");
    foreach ($agences as $fields) {
        $agenceX = [];
        $agenceX[] = $fields->getNom();
        $agenceX[] = $fields->getCode();
        $agenceX[] = $fields->getAdresse();
        $agenceX[] = $fields->getCodePostal();
        fputcsv($fp, $agenceX, ";");
    }
    fclose($fp);
}
function creationCsvComptes($comptes)
{
    $fp = fopen("./CSV/infos_comptes.csv", "w+");
    foreach ($comptes as $fields) {
        $compteX = [];
        $compteX[] = $fields->getCompteCodeAgence();
        $compteX[] = $fields->getNumero();
        $compteX[] = $fields->getId();
        $compteX[] = $fields->getSolde();
        $compteX[] = $fields->getType();
        fputcsv($fp, $compteX, ";");
    }
    fclose($fp);
}
