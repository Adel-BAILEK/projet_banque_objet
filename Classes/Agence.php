<?php



class Agence
{
    private string $nom;
    private int $code;
    private string $adresse;
    private int $codePostal;


public function __construct(string $nom,int $code, string $adresse , int $codePostal)
{
    $this->nom = $nom;
    $this->code = $code;
    $this->adresse = $adresse;
    $this->codePostal = $codePostal;
}

public function getNom()
{
    return $this->nom;
}
public function setNom($nom)
{
    $this->nom = $nom;
}
public function getCode()
{
    return $this->code;
}
public function getAdresse()
{
    return $this->adresse;
}
public function getCodePostal()
{
    return $this->codePostal;
}

}